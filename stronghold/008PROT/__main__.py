from stronghold.utils.stdinreader import read_stdin, read_one_line
from stronghold.utils.seq import RNASequence

seq = RNASequence("", read_stdin(read_one_line))
print(seq.protein())
