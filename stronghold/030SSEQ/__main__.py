import sys

from stronghold.utils.fasta import parse_fasta
from stronghold.utils.seq import DNASequence
from stronghold.utils import print_nums

sequence, motif = parse_fasta(DNASequence, sys.stdin.readlines())

index_in_seq = 0
indexes = []
for base in motif.seq:
	offset = sequence.seq[index_in_seq:].index(base)
	index_in_seq += offset + 1
	indexes.append(index_in_seq)

print_nums(indexes)
