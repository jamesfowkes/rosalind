import sys

from stronghold.utils.fasta import parse_fasta
from stronghold.utils.seq import Sequence

lines = sys.stdin.readlines()
sequences = parse_fasta(Sequence, lines)

highest_gc = 0.0
highest_id = ""

for sequence in sequences:
    gc_content = sequence.gc_content()
    if highest_gc < gc_content:
        highest_gc = gc_content
        highest_id = sequence.name

print(highest_id)   
print(highest_gc)
