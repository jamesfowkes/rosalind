from itertools import permutations
from stronghold.utils.stdinreader import read_stdin, read_parts

n = tuple(read_stdin(read_parts(int, 1)))[0]

all_permutations = list(permutations(range(1, n+1)))

print(len(all_permutations))
for perm in all_permutations:
	print(" ".join([str(p) for p in perm]))
