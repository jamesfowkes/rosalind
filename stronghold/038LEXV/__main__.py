from itertools import product

from stronghold.utils import print_array_lines
from stronghold.utils.stdinreader import read_stdin, read_one_line, read_parts

alphabet, max_length = read_stdin([read_one_line, read_parts(int, 1)])
alphabet = alphabet.replace(" ", "")

combinations = []
for length in range(1, max_length+1):
	for combination in product(alphabet, repeat=length):
		combinations.append("".join(combination))

sorted_combinations = sorted(combinations, key=lambda comb: [alphabet.index(c) for c in comb]) 
print_array_lines(sorted_combinations)
