import sys

from stronghold.utils.fasta import parse_fasta
from stronghold.utils.seq import DNASequence

lines = sys.stdin.readlines()
sequence = parse_fasta(DNASequence, lines)

proteins = set([orf.protein().seq for orf in sequence.open_reading_frames()])

for protein in proteins:
	print(protein)
