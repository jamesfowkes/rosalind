import sys

from stronghold.utils.fasta import parse_fasta
from stronghold.utils.seq import Sequence
from stronghold.utils.array_utils import make_array

def get_lcss_length_table(s1, s2):
    
    len_s1 = len(s1)
    len_s2 = len(s2)

    cache = make_array(len_s1+1, len_s2+1)

    for i in range(len_s1+1):
        cache[i][0] = 0

    for j in range(len_s2+1):
        cache[0][j] = 0

    for i in range(1, len_s1+1):
        for j in range(1, len_s2+1):
            c1 = s1[i-1]
            c2 = s2[j-1]

            if c1 != c2:
                cache[i][j] = max(cache[i-1][j], cache[i][j-1])
            elif c1 == c2:
                cache[i][j] = cache[i-1][j-1] + 1

    return cache

def get_one_css(s1, s2, i=None, j=None, table=None):

    if table is None:
        table = get_lcss_length_table(s1, s2)

    if i is None:
        i = len(s1)

    if j is None:
        j = len(s2)

    if i == -1 or j == -1:
        return ""

    c1 = s1[i-1]
    c2 = s2[j-1]
    if c1 == c2:
        return get_one_css(s1, s2, i-1, j-1, table) + c1

    if table[i][j-1] > table[i-1][j]:
        return get_one_css(s1, s2, i, j-1, table)
    else:
        return get_one_css(s1, s2, i-1, j, table)

#def get_all_css(s1, s2, i=None, j=None, table=None):
#
#    if table is None:
#        table = get_lcss_length_table(s1, s2)
#
#    if i is None:
#        i = len(s1)
#
#    if j is None:
#        j = len(s2)
#
#    if i == -1 or j == -1:
#        return {""}
#
#    if i in get_all_css.cache and j in get_all_css.cache[i]:
#        # print("Result for {},{} found in cache: {}".format(i,j, get_all_css.cache[i][j]))
#        return get_all_css.cache[i][j]
#    elif i not in get_all_css.cache:
#        get_all_css.cache[i] = {}
#
#    try:
#        c1 = s1[i-1]
#        c2 = s2[j-1]
#        if c1 == c2:
#            #print("Equal chars at {},{}".format(i,j))
#            result = {Z + c1 for Z in get_all_css(s1, s2, i-1, j-1, table)}
#            get_all_css.cache[i][j] = result
#            return result
#
#        charset = set()
#        if table[i][j-1] >= table[i-1][j]:
#            #print("Unequal chars ({},{}) at {},{} and {}>={}".format(
#            #    c1, c2, i, j, table[i][j-1], table[i-1][j])
#            #)
#            charset = charset | get_all_css(s1, s2, i, j-1, table)
#            get_all_css.cache[i][j] = charset
#            
#        if table[i-1][j] >= table[i][j-1]:
#            #print("Unequal chars ({},{}) at {},{} and {}>={}".format(
#            #    c1, c2, i, j, table[i-1][j], table[i][j-1])
#            #)
#            charset = charset | get_all_css(s1, s2, i-1, j, table)
#            get_all_css.cache[i][j] = charset
#
#    except RecursionError:
#        print("i = {}, j = {}".format(i, j))
#        raise
#
#    return charset
#
#get_all_css.cache = {}

sys.setrecursionlimit(sys.getrecursionlimit() * 2)

lines = sys.stdin.readlines()
sequences = parse_fasta(Sequence, lines)

one_subseq = get_one_css(sequences[0].seq, sequences[1].seq)
print(one_subseq)
