import unittest

from fib import run_months

class REVCTest(unittest.TestCase):

    def testRunMonthsWithZeroInputProduces1Pair(self):
        self.assertEqual(1, run_months(0, 0))

    def testRunMonthsWithNoPairProductionProducesConstantOutput(self):
        self.assertEqual(1, run_months(10, 0))

    def testRunMonthsWithOnePairProductionProducesFibbonacciSequence(self):
        self.assertEqual(1, run_months(1, 1))
        self.assertEqual(1, run_months(2, 1))
        self.assertEqual(2, run_months(3, 1))
        self.assertEqual(3, run_months(4, 1))
        self.assertEqual(5, run_months(5, 1))

    def testRunMonthsWithExamplePairProductionProducesCorrectSequence(self):
        self.assertEqual(1, run_months(1, 3))
        self.assertEqual(1, run_months(2, 3))
        self.assertEqual(4, run_months(3, 3))
        self.assertEqual(7, run_months(4, 3))
        self.assertEqual(19, run_months(5, 3))

if __name__ == '__main__':
    unittest.main()