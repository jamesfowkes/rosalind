def run_months(n_months, pairs_per_litter):

    try:
        return run_months.cache[pairs_per_litter][n_months]
    except KeyError:
        pass

    if pairs_per_litter == 0:
        return 1

    if n_months in (1, 2):
        return 1
    else:
        result = (pairs_per_litter * run_months(n_months-2, pairs_per_litter)) + run_months(n_months-1, pairs_per_litter)
        if pairs_per_litter in run_months.cache:
            run_months.cache[pairs_per_litter][n_months] = result
        else:
            run_months.cache[pairs_per_litter] = {n_months:result}

        return result

run_months.cache = {}
