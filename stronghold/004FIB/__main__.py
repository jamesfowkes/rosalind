from stronghold.utils.stdinreader import read_stdin, read_parts
from .FIB import run_months

n_months, pairs_per_litter = read_stdin(read_parts(int, 2))

print(run_months(n_months, pairs_per_litter))
