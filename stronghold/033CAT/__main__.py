import sys

from stronghold.utils.fasta import parse_fasta
from stronghold.utils.seq import RNASequence

sequence = parse_fasta(RNASequence, sys.stdin.readlines())

SPACER = "    "

def count_perfect_matchings(seq):

	n = len(seq)
	count_perfect_matchings.recurse += 1

	count = 0

	if n == 0:
		count = 1

	elif n == 1:
		count = 0

	elif seq in count_perfect_matchings.cache:
		count = count_perfect_matchings.cache[seq]

	else:
		# for split_at in range(1, n+1):
		# 	count = 0
		# 	if valid_match_fn(split_at-1, n-split_at):
		# 		count += count_perfect_matchings(split_at-1) * count_perfect_matchings(n-split_at)

		count = 0

		for k in range(1, n+1):
			if valid_match_fn(seq, k, n):
				r1 = count_perfect_matchings(seq[0:k-1])
				r2 = count_perfect_matchings(seq[k:n-1])
				if r1 and r2:
					count += (r1 * r2)


		count_perfect_matchings.cache[seq] = count

	count_perfect_matchings.recurse -= 1
	return count

count_perfect_matchings.recurse = -1


def valid_match_fn(seq, k, n):
	match = RNASequence("", seq).match_bases(k-1, n-1)
	return match

count_perfect_matchings.cache = {"AU": 1, "UA": 1, "CG": 1, "GC": 1}
count_perfect_matchings.valid_match_fn = valid_match_fn

print(count_perfect_matchings(sequence.seq) % 1000000)
