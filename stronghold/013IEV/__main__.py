from stronghold.utils.stdinreader import read_stdin, read_parts

couples = tuple(read_stdin(read_parts(int, 6)))

weights = (2, 2, 2, 1.5, 1, 0)

print(sum([c * w for c, w in zip(couples, weights)]))
