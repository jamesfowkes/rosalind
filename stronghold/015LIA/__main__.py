from stronghold.utils import pairwise
from stronghold.utils.stdinreader import read_stdin, read_parts

generation, N = tuple(read_stdin(read_parts(int, 2)))

n_children = pow(2, generation)

def pascal(n):
    if n == 1:
        return (1)
    elif n == 2:
        return (1, 1)
    else:
        prev = pascal(n-1)
        new = tuple(n1 + n2 for n1, n2 in pairwise(prev))
        return (1, *new, 1)

coefficients = pascal(n_children+1)

cumulative_prob = 0
for n in range(N, n_children+1):
    prob = pow(3, n_children-n) * coefficients[n]
    cumulative_prob += prob
    
divisor = pow(4, n_children)
print(cumulative_prob / divisor)
