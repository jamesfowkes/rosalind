import sys

from stronghold.utils.fasta import parse_fasta
from stronghold.utils.seq import DNASequence


sequences = parse_fasta(DNASequence, sys.stdin.readlines())

valid_indexes = []

for index, sequence1 in enumerate(sequences):
	if index not in valid_indexes:
		for index2, sequence2 in enumerate(sequences[index+1:]):
			if sequence1.match(sequence2):
				valid_indexes.append(index)
				valid_indexes.append(index+index2+1)

invalid_sequences = []
valid_sequences = []

for i in range(0, len(sequences)):
	if i not in valid_indexes:
		invalid_sequences.append(sequences[i])
	else:
		valid_sequences.append(sequences[i])

for invalid_seq in invalid_sequences:
	for valid_seq in valid_sequences:
		if invalid_seq.hamming_distance(valid_seq) == 1:
			print("{}->{}".format(invalid_seq.seq, valid_seq.seq))
			break
		elif invalid_seq.hamming_distance(valid_seq.reverse_complement().seq) == 1:
			print("{}->{}".format(invalid_seq.seq, valid_seq.reverse_complement().seq))
			break