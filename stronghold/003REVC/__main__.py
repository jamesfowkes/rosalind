from stronghold.utils.stdinreader import read_stdin, read_one_line
from stronghold.utils.seq import Sequence

seq = Sequence("", read_stdin(read_one_line))
print(seq.reverse_complement())
