from itertools import product, permutations

from stronghold.utils.stdinreader import read_stdin, read_parts
from stronghold.utils import print_nums

count = read_stdin(read_parts(int, 1))

multipliers = list(product([-1, 1], repeat=count))

positive_integers = list(range(1, count+1))

all_permutations = []
for multiplier in multipliers:
    result = [x*y for x, y in zip(multiplier, positive_integers)]

    for p in permutations(result):
        all_permutations.append(p)

print(len(all_permutations))

for p in all_permutations:
    print_nums(p)
