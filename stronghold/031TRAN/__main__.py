import sys

from stronghold.utils.fasta import parse_fasta
from stronghold.utils.seq import DNASequence

sequences = parse_fasta(DNASequence, sys.stdin.readlines())

print(sequences[0].tt_ratio(sequences[1]))
