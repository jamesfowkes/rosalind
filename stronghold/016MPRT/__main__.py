import regex as re

from stronghold.utils.uniprot import uniprot_get_fasta
from stronghold.utils.stdinreader import readlines

n_glycosylation_regex = re.compile("N[^P][ST][^P]")

lines = readlines()

sequences = {access_id: uniprot_get_fasta(access_id) for access_id in lines}

for access_id, seq in sequences.items():
    matches = n_glycosylation_regex.finditer(seq.seq, overlapped=True)
    to_print = " ".join([str(m.span()[0]+1) for m in matches])

    if len(to_print):
        print(access_id)
        print(to_print)
