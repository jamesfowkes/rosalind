from stronghold.utils.stdinreader import read_stdin, read_one_line
from stronghold.utils.seq import Sequence

seq, motif = read_stdin([read_one_line, read_one_line])
seq = Sequence("", seq)
locations = seq.find(motif)
print(" ".join([str(loc+1) for loc in locations]))
