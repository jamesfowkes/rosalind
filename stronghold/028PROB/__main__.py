import math

from stronghold.utils.stdinreader import read_stdin, read_one_line, read_parts

def get_probability(base, gc):
	if base in ['A', 'T']:
		return (1-gc)/2
	elif base in ['C', 'G']:
		return gc/2

def get_gc_probability(seq, gc_content):
	prob = 0
	for base in sequence:
		prob += math.log(get_probability(base, gc_content), 10)

	return prob

sequence, gc_contents = read_stdin([read_one_line, read_parts(float)])

probs = [get_gc_probability(sequence, gc_content) for gc_content in gc_contents]
	
print(" ".join(["{:.3f}".format(p) for p in probs]))