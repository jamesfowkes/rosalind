import unittest

from RNA import dna_to_rna

class RNATest(unittest.TestCase):

    def testZeroLengthStringReturnsZeroLengthString(self):
        assert "" == dna_to_rna("")

    def testInvalidStringReturnsSameString(self):
        assert "BDEFHIJKLMNOPQRSUVWXYZ" == dna_to_rna("BDEFHIJKLMNOPQRSUVWXYZ")

    def testValidStringReturnsCorrectlyTranscribedString(self):
        assert "GAUGGAACUUGACUACGUAAAUU" == dna_to_rna("GATGGAACTTGACTACGTAAATT")

if __name__ == '__main__':
    unittest.main()