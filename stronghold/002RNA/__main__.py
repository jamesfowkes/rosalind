from stronghold.utils.stdinreader import read_stdin, read_one_line
from stronghold.utils.seq import DNASequence

dna = DNASequence("", read_stdin(read_one_line))

print(dna.to_rna())
