from collections import namedtuple

Generation = namedtuple("Generation", ["born", "died", "alive"])

def run_months(n_months, lifespan):

    try:
        result = run_months.cache[n_months]
    except KeyError:
        run_months(n_months-1, lifespan)

        try:
            died_this_month = run_months.cache[n_months-lifespan].born
        except KeyError:
            died_this_month = 0

        born = run_months.cache[n_months-2].alive - run_months.cache[n_months-1].died
        alive_last_month = run_months.cache[n_months-1].alive

        result = Generation(born, died_this_month, alive_last_month+born-died_this_month)

    run_months.cache[n_months] = result
    return result

run_months.cache = {1: Generation(1, 0, 1), 2: Generation(0, 0, 1)}
