from stronghold.utils.stdinreader import read_stdin, read_parts
from .FIBD import run_months

n_months, lifespan = read_stdin(read_parts(int, 2))

print(run_months(n_months, lifespan).alive)
