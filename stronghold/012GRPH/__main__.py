import sys

from stronghold.utils.fasta import parse_fasta
from stronghold.utils.seq import Sequence

lines = sys.stdin.readlines()
sequences = parse_fasta(Sequence, lines)

def find_seqs_with_prefix(prefix, sequences):
    matches = []
    for i, seq in enumerate(sequences):
        if seq.seq.startswith(prefix):
            matches.append(seq)

    return matches

def get_adjacency_list(sequences):
    adjacency_list = {}
    for i, seq in enumerate(sequences):
        prefix = seq.seq[-3:]
        others = sequences[0:i] + sequences[i+1:]
        matches = find_seqs_with_prefix(prefix, others)
        adjacency_list[seq] = matches

    return adjacency_list

adjacency_list = get_adjacency_list(sequences)

for seq, overlaps in adjacency_list.items():
    for overlap in overlaps:
        print(seq.name + " " + overlap.name)