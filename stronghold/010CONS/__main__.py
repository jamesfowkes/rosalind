import sys

from stronghold.utils.fasta import parse_fasta
from stronghold.utils.seq import Sequence, ProfileMatrix

lines = sys.stdin.readlines()
sequences = parse_fasta(Sequence, lines)

matrix = ProfileMatrix(sequences)

print(matrix.consensus.seq)

print("A: " + " ".join([str(i) for i in matrix.matrix[0]]))
print("C: " + " ".join([str(i) for i in matrix.matrix[1]]))
print("G: " + " ".join([str(i) for i in matrix.matrix[2]]))
print("T: " + " ".join([str(i) for i in matrix.matrix[3]]))
