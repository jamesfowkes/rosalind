from stronghold.utils.stdinreader import read_stdin, read_one_line
from stronghold.utils.seq import ProteinSequence

seq = ProteinSequence("", read_stdin(read_one_line))

count = 1
for i in range(len(seq)):
    count *= len(seq.get_candidate_rna(i))

print((count * 3) % 1000000)
