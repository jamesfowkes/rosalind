from stronghold.utils.stdinreader import read_stdin, read_parts

num_nodes, adjacency_list = read_stdin(read_parts(int, 1), repeat_to_end_fn=read_parts(int, 2))

print(num_nodes - 1 - len(adjacency_list))
