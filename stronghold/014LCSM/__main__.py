import sys

from stronghold.utils.fasta import parse_fasta
from stronghold.utils.seq import Sequence

def all_substrings(sequence, length):
    max_index = len(sequence)-length+1
    return [sequence[i:i+length] for i in range(max_index)]

def find_lcm(sequences, length):
    for substring in all_substrings(sequences[0].seq, length):
        if all(substring in seq.seq for seq in sequences):
            return substring

lines = sys.stdin.readlines()
sequences = parse_fasta(Sequence, lines)

last_lcm = ""
length = 1
while True:
    this_lcm = find_lcm(sequences, length)
    if not this_lcm:
        print(last_lcm)
        break
    else:
        last_lcm = this_lcm
        length += 1
