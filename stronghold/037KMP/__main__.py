import sys
import regex as re

from itertools import product

from stronghold.utils.fasta import parse_fasta
from stronghold.utils.seq import RNASequence
from stronghold.utils import print_nums
from stronghold.utils import longest_matching_suffixprefix, failure_array

sequence = parse_fasta(RNASequence, sys.stdin.readlines())
_failure_array = failure_array(sequence)

#print(" ".join(sequence))
print_nums(_failure_array)
