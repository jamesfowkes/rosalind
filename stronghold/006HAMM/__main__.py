import sys

from stronghold.utils.seq import Sequence

sequences = sys.stdin.readlines()

s1 = Sequence("", sequences[0])
s2 = Sequence("", sequences[1])

print(s1.hamming_distance(s2))
