from stronghold.utils.stdinreader import read_stdin, read_one_line, read_parts
from stronghold.utils import print_nums

def get_subsequence_counts(elements, comparator):

    length = len(elements)
    counts = [1] * len(elements)

    for current_index in range(1, length):
        for s in range(0, current_index):
            if comparator(elements[s], elements[current_index]):
                counts[current_index] = max(counts[current_index], counts[s] + 1)

    return counts


def get_longest_subsequence(elements, counts):

    elements = elements[::-1]
    counts = counts[::-1]

    max_count = max(counts)

    search_start = 0
    find_index = 0
    subsequence = []
    for i in range(max_count, 0, -1):
        find_index = search_start + counts[search_start:].index(i)
        search_start = find_index + 1
        subsequence.append(elements[find_index])

    return subsequence[::-1]

_, elements = read_stdin([read_one_line, read_parts(int)])

counts = get_subsequence_counts(elements, lambda a, b: a < b)
print_nums(get_longest_subsequence(elements, counts))

counts = get_subsequence_counts(elements, lambda a, b: a > b)
print_nums(get_longest_subsequence(elements, counts))
