from itertools import combinations

def build_list(k, m, n):

    lst = ['d'] * k
    lst.extend(['h'] * m)
    lst.extend(['r'] * n)

    return lst


from stronghold.utils.stdinreader import read_stdin, read_parts

k, m, n = read_stdin(read_parts(int, 3))

population = build_list(int(k), int(m), int(n))
matings = list(combinations(population, 2))

count = 0
for m in matings:
    if 'd' in m:
        count += 1
    elif m == ('h', 'h'):
        count += 0.75
    elif m == ('h', 'r'):
        count += 0.5

print(count/len(matings))
