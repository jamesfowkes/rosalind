import sys
import regex as re

from itertools import product

from stronghold.utils.fasta import parse_fasta
from stronghold.utils.seq import RNASequence
from stronghold.utils import print_nums

sequence = parse_fasta(RNASequence, sys.stdin.readlines())

kmers = sorted(["". join(p) for p in product("ACGT", repeat=4)])

counts = []
for k in kmers:
    regex = re.compile(k)
    counts.append(len(list(regex.finditer(sequence.seq, overlapped=True))))

print_nums(counts, " ")
