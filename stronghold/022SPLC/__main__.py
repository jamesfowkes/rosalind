import sys

from stronghold.utils.fasta import parse_fasta
from stronghold.utils.seq import DNASequence

lines = sys.stdin.readlines()
sequences = parse_fasta(DNASequence, lines)

full_seq = sequences[0]
introns = sequences[1:]

for intron in introns:
    new_seq = full_seq.seq.replace(intron.seq, "")
    full_seq = DNASequence(full_seq.name, new_seq)

print(full_seq.protein().seq)
