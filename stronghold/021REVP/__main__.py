import sys

from stronghold.utils.fasta import parse_fasta
from stronghold.utils.seq import DNASequence

lines = sys.stdin.readlines()
sequence = parse_fasta(DNASequence, lines)

for length in range(4, 13):
    for index in range(0, len(sequence)-length+1):
        subseq = sequence.seq[index:index+length]
        if DNASequence("", subseq).reverse_complement().seq == subseq:
            print(index+1, length)
