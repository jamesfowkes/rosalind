import math
from stronghold.utils.stdinreader import read_stdin, read_parts

p, r = read_stdin(read_parts(int, 2))

npr = int((math.factorial(p) / math.factorial(p-r)) % 1000000)

print(npr)
