import sys
from dataclasses import dataclass
from functools import total_ordering

from stronghold.utils.fasta import parse_fasta
from stronghold.utils.seq import DNASequence

sequences = parse_fasta(DNASequence, sys.stdin.readlines())

@dataclass
@total_ordering

class Overlap:
    s1: str
    s2: str
    common_seq: str
    s1_overlap_start: int

    def __len__(self):
        return len(self.common_seq)

    def __eq__(self, other):
        return self.common_seq == other.common_seq

    def __lt__(self, other):
        return len(self) < len(other)

    def __hash__(self):
        return hash(self.common_seq)

    def merged(self):
        return self.s1_only() + self.common_seq + self.s2_only()

    def s1_only(self):
        return self.s1[0:self.s1_overlap_start]

    def s2_only(self):
        return self.s2[len(self):]

def get_overlap(seq1, seq2):

    overlaps =[]

    length = len(seq1) - 1
    for i in range(1, length):
        cmp1 = seq1[i:]
        cmp2 = seq2[0:len(cmp1)]
        if cmp1 == cmp2:
            overlaps.append(Overlap(seq1, seq2, cmp1, i))
        length = -1

    if len(overlaps):
        return max(overlaps)

def get_candidate_overlaps(sequences):
    overlaps = []
    overlap = None
    for i1, seq1 in enumerate(sequences):
        for i2, seq2 in enumerate(sequences):
            length_threshold = min(len(seq1), len(seq2)) / 2
            if seq1 != seq2:
                overlap_1_to_2 = get_overlap(seq1.seq, seq2.seq)
                overlap_2_to_1 = get_overlap(seq2.seq, seq1.seq)

                if overlap_1_to_2 and overlap_2_to_1:
                    overlap = max(overlap_1_to_2, overlap_2_to_1)
                elif overlap_1_to_2:
                    overlap = overlap_1_to_2
                elif overlap_2_to_1:
                    overlap = overlap_2_to_1

                if overlap and (len(overlap) > length_threshold):
                    overlaps.append(overlap)

    return set(overlaps)

overlaps = list(get_candidate_overlaps(sequences))
sequence = overlaps.pop(0).merged()

while len(overlaps):
    for index, overlap in enumerate(overlaps):
        if sequence.startswith(overlap.common_seq):
            sequence = overlap.s1_only() + sequence
            overlaps.pop(index)
            break
        if sequence.endswith(overlap.common_seq):
            sequence = sequence + overlap.s2_only()
            overlaps.pop(index)
            break

print(sequence)
