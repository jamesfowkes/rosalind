import sys
import os
import importlib
import runpy
import pathlib

from dataclasses import dataclass
from pathlib import Path

THIS_PATH = Path(__file__).parent

def is_problem_dir(d):
    return os.path.isdir(d) and str(d.name)[0:3].isdigit()

@dataclass
class Problem:
    numeric_id : str
    name_id: str

    def full_name(self):
        return self.numeric_id + self.name_id

    def input_file(self, filename="input.txt"):
        return pathlib.Path(THIS_PATH, self.full_name(), filename)

    def module(self):
        return "stronghold.{}".format(self.full_name())

    def __str__(self):
        return self.full_name()

    @classmethod
    def find_by_numeric_id(cls, num_id):
        dirs = os.listdir(THIS_PATH)

        num_id = "{:03d}".format(int(num_id))
        for d in dirs:
            if d.startswith(num_id):
                module_name = d[len(num_id):]
                return cls(num_id, module_name)

        return None

    @classmethod
    def find_all(cls):
        dirs = os.listdir(THIS_PATH)
        
        problems = []
        for d in dirs:
            if is_problem_dir(pathlib.Path(THIS_PATH, d)):
                num_id = d[0:3]
                module_name = d[3:]
                problems.append(cls(num_id, module_name))

        return problems

input_filename = "input.txt"

try:
    problems = [Problem.find_by_numeric_id(sys.argv[1])]
    try:
        input_filename = sys.argv[2]
    except IndexError:
        pass

except IndexError:
    problems = Problem.find_all()
    print(problems)

for problem in problems:
    print("Running {} with {}".format(problem, input_filename))

    with open(problem.input_file(input_filename), 'r') as input_file:
        sys.stdin = input_file
        runpy.run_module(problem.module())
