from itertools import product

from stronghold.utils.stdinreader import read_stdin, read_one_line, read_parts

alphabet, length = read_stdin([read_one_line, read_parts(int, 1)])

for c in product(alphabet, repeat=length):
    print("".join(c))
