import unittest

from seq import Sequence
from fasta import parse_fasta

class SeqTest(unittest.TestCase):

    def testSingleLineSequenceParsedCorrectly(self):
        parsed = parse_fasta(Sequence, [">TestName", "CCTGCGGAAGATCGGCACTAGAATAGCCAGAACCGTTTCTCTGAGGCTTCCGGCCTTCCC"])
        self.assertEqual("TestName", parsed.name)
        self.assertEqual("CCTGCGGAAGATCGGCACTAGAATAGCCAGAACCGTTTCTCTGAGGCTTCCGGCCTTCCC", parsed.seq)

    def testMultiLineSequenceParsedCorrectly(self):
        parsed = parse_fasta(Sequence, [">TestName", "CCTGCGGAAGATCGGCACTAGAATAGCC", "AGAACCGTTTCTCTGAGGCTTCCGGCCTTCCC"])
        self.assertEqual("TestName", parsed.name)
        self.assertEqual("CCTGCGGAAGATCGGCACTAGAATAGCCAGAACCGTTTCTCTGAGGCTTCCGGCCTTCCC", parsed.seq)

    def testSeveralMultiLineSequencesParsedCorrectly(self):
        parsed = parse_fasta(Sequence, [
            ">TestName", "CCTGCGGAAGATCGGCACTAGAATAGCC", "AGAACCGTTTCTCTGAGGCTTCCGGCCTTCCC",
            ">TestName2", "AGAACCGTTTCTCTGAGGCTTCCGGCCTTCCC", "CCTGCGGAAGATCGGCACTAGAATAGCC"
            ]
        )
        self.assertEqual("TestName", parsed[0].name)
        self.assertEqual("CCTGCGGAAGATCGGCACTAGAATAGCCAGAACCGTTTCTCTGAGGCTTCCGGCCTTCCC", parsed[0].seq)
        self.assertEqual("TestName2", parsed[1].name)
        self.assertEqual("AGAACCGTTTCTCTGAGGCTTCCGGCCTTCCCCCTGCGGAAGATCGGCACTAGAATAGCC", parsed[1].seq)

if __name__ == '__main__':
    unittest.main()