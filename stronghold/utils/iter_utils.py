import itertools

def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = itertools.tee(iterable)
    next(b, None)
    return zip(a, b)

def longest_length_filter(iterable, last_length):
    new_length = len(iterable)

    if new_length > last_length:
        return 1, new_length
    elif new_length == last_length:
        return 0, new_length
    else:
        return -1, last_length

def dynamic_filter(iterable, start_value, filter_fn):

    """ filter_fn should return :
    <0 if item should not be kept
    0 if item should be kept
    >0 if new list should be started
    """

    indexes = []
    items = []
    last_value = start_value

    for index, item in enumerate(iterable):
        result, last_value = filter_fn(item, last_value)
        if result > 0:
            indexes = [index]
            items = [item]
        elif result == 0:
            indexes.append(index)
            items.append(item)

    return items, indexes
