import sys

try:
    from .seq import Sequence
except:
    from seq import Sequence

def get_name(line):
    return line[1:].strip()

def parse_fasta(sequence_class, text_or_lines):

    if type(text_or_lines) is str:
        lines = text_or_lines.splitlines()
    else:
        lines = text_or_lines

    sequences = []
    this_seq = ""
    this_name = get_name(lines[0])

    for l in lines[1:]:
        if l.startswith(">"):
            sequences.append(sequence_class(this_name, this_seq.strip()))
            this_name = get_name(l)
            this_seq = ""
        else:
            this_seq += l

    if this_seq != "":
        sequences.append(sequence_class(this_name, this_seq.strip()))

    if len(sequences) > 1:
        return sequences
    else:
        return sequences[0]

if __name__ == "__main__":
    lines = sys.stdin.readlines()
    sequences = parse_fasta(Sequence, lines)

    print("Got {} sequences:".format(len(sequences)))
    
    for seq in sequences:
        print(seq.name + ":")
        print(seq.seq)
