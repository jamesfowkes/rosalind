import unittest

from seq import Sequence, DNASequence, ProfileMatrix, ReadingFrame

class SeqTest(unittest.TestCase):

    def testZeroLengthStringReturnsZeroLengthString(self):
        self.assertEqual("", Sequence("", "").seq)

    def testInvalidStringReturnsSameStringWhenReversed(self):
        self.assertEqual(Sequence("", "ZYXWVUSRQPONMLKJIHFEDB", True), Sequence("", "BDEFHIJKLMNOPQRSUVWXYZ").reverse_complement())

    def testMultiLineStringCreatesSingleSequence(self):
        self.assertEqual("CCACCCTCGTGGTATGGCTAGGCATTCAGGAACC", Sequence("", "CCACCC\r\nTCGTGGTAT\nGGCTAGGCATTCAGGAACC").seq)

    def testValidStringReturnsCorrectlyComplementedString(self):
        self.assertEqual(Sequence("", "ACCGGGTTTT", True), Sequence("", "AAAACCCGGT", False).reverse_complement())

    def testGetGCContentOfValidStringReturnsCorrectValue(self):
        seq = "CCACCCTCGTGGTATGGCTAGGCATTCAGGAACCGGAGAACGCTTCAGACCAGCCCGGACTGGGAACCTGCGGGCAGTAGGTGGAAT"
        self.assertAlmostEqual(60.919540, Sequence("Rosalind_0808", seq).gc_content(), places=3)

    def testLengthOfSequenceIsCorrect(self):
        self.assertEqual(0, len(Sequence("", "")))
        self.assertEqual(1, len(Sequence("", "A")))
        self.assertEqual(10, len(Sequence("", "AAAACCCGGT")))

    def testHammingDistanceWithUnequalLengthStringsRaisesException(self):
        with self.assertRaises(Exception):
            Sequence("", "AAAACCCGGT").hamming_distance(Sequence("", "AAAACCCGG"))

    def testHammingDistanceIsCalculatedCorrectlyWithSequencesAndStrings(self): 
        self.assertEqual(7, Sequence("", "GAGCCTACTAACGGGAT").hamming_distance(Sequence("", "CATCGTAATGACGGCCT")))
        self.assertEqual(7, Sequence("", "GAGCCTACTAACGGGAT").hamming_distance("CATCGTAATGACGGCCT"))

class DNASequenceTest(unittest.TestCase):

    def testFindStartCodonsReturnsCorrectResults(self):
        self.assertEqual((4, 24, 30), DNASequence("", "AGCCATGTAGCTAACTCAGGTTACATGGGGATGACCCCGCGACTTGGAT").find_start_codons())

    def testFindStopCodonsReturnsCorrectResults(self):
        self.assertEqual((7, 11, 31), DNASequence("", "AGCCATGTAGCTAACTCAGGTTACATGGGGATGACCCCGCGACTTGGAT").find_stop_codons())

    def testGetOpenReadingFramesReturnsEmptyTupleForNoORFs(self):
        self.assertEqual((), DNASequence("", "AAAAAAAAAA").open_reading_frames())
        self.assertEqual((), DNASequence("", "TAGAAAAAAA").open_reading_frames())
        self.assertEqual((), DNASequence("", "AAAAAAATAG").open_reading_frames())
        self.assertEqual((), DNASequence("", "AAAAAAATGA").open_reading_frames())
        self.assertEqual((), DNASequence("", "AAAAAAATAA").open_reading_frames())

    def testGetOpenReadingFramesReturnsCorrectORFWithOneORF(self):
        input_seq = DNASequence("Test1A", "ATGTAG")
        expected = ReadingFrame(input_seq, 0, 3, False, "ATGTAG")
        self.assertEqual((expected,), input_seq.open_reading_frames())

        input_seq = DNASequence("Test1B", "ATGCGTGGCAGGTAG")
        expected = ReadingFrame(input_seq, 0, 12, False, "ATGCGTGGCAGGTAG")
        self.assertEqual((expected,), input_seq.open_reading_frames())

        input_seq = DNASequence("Test1C", "CCCCCCATGCGTGGCAGGTAG")
        expected = ReadingFrame(input_seq, 6, 18, False, "ATGCGTGGCAGGTAG")
        self.assertEqual((expected,), input_seq.open_reading_frames())

    def testGetOpenReadingFramesReturnsCorrectORFsWithMultipleORFs(self):
        input_seq = DNASequence("Test2A", "ATGTAGATGTAG")
        expected = (
            ReadingFrame(input_seq, 0, 3, False, "ATGTAG"),
            ReadingFrame(input_seq, 6, 9, False, "ATGTAG")
        )
        self.assertEqual(expected, input_seq.open_reading_frames())
        
        input_seq = DNASequence("Test2B", "ATGCATGGGTAGCCTGTAA")
        expected = (
            ReadingFrame(input_seq, 0 ,9, False, "ATGCATGGGTAG"),
            ReadingFrame(input_seq, 4 ,16, False, "ATGGGTAGCCTGTAA")
        )
        self.assertEqual(expected, input_seq.open_reading_frames())

class ProfileMatrixTest(unittest.TestCase):

    def testProfileMatrixWithOneSequence(self):

        sequences = [Sequence("", "ATCCAGCT")]
        result = ProfileMatrix(sequences)

        expected_profile_matrix = (
            (
                (1, 0, 0, 0, 1, 0, 0, 0),
                (0, 0, 1, 1, 0, 0, 1, 0),
                (0, 0, 0, 0, 0, 1, 0, 0),
                (0, 1, 0, 0, 0, 0, 0, 1)
            )
        )

        expected_consensus = Sequence("", "ATCCAGCT")
 
        self.assertEqual(expected_profile_matrix, result.matrix)
        self.assertEqual(expected_consensus, result.consensus)

    def testProfileMatrixWithMultipleSequences(self):

        sequences = [
            Sequence("", "ATCCAGCT"),
            Sequence("", "GGGCAACT"),
            Sequence("", "ATGGATCT"),
            Sequence("", "AAGCAACC"),
            Sequence("", "TTGGAACT"),
            Sequence("", "ATGCCATT"),
            Sequence("", "ATGGCACT")
        ]
 
        result = ProfileMatrix(sequences)
        
        expected_profile_matrix = (
            (5, 1, 0, 0, 5, 5, 0, 0),
            (0, 0, 1, 4, 2, 0, 6, 1),
            (1, 1, 6, 3, 0, 1, 0, 0),
            (1, 5, 0, 0, 0, 1, 1, 6)
        )

        expected_consensus = Sequence("", "ATGCAACT")
 
        self.assertEqual(expected_profile_matrix, result.matrix)
        self.assertEqual(expected_consensus, result.consensus)


if __name__ == '__main__':
    unittest.main()