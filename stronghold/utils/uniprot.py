import requests
import requests_cache

from .seq import Sequence
from .fasta import parse_fasta

requests_cache.install_cache('uni_prot.cache')

def uniprot_get_fasta(access_id):

	URL = "http://www.uniprot.org/uniprot/{}.fasta".format(access_id)
	result = requests.get(URL)
	if result.status_code == 200:
		return parse_fasta(Sequence, result.text)
	else:
		raise Exception("Status code {} looking up {}".format(result.status_code, access_id))

if __name__ == "__main__":

	import sys
	print(uniprot_get_fasta(sys.argv[1]))
