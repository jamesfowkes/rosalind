def complement_base(base):
    if base == 'A':
        return 'T'
    elif base == 'T':
        return 'A'
    elif base == 'C':
        return 'G'
    elif base == 'G':
        return 'C'

    return base

def match_dna(base1, base2):
    match = base1 == "A" and base2 == "T"
    match |= base1 == "C" and base2 == "G"
    match |= base1 == "G" and base2 == "C"
    match |= base1 == "T" and base2 == "A"
    return match

def match_rna(base1, base2):
    match = base1 == "A" and base2 == "U"
    match |= base1 == "C" and base2 == "G"
    match |= base1 == "G" and base2 == "C"
    match |= base1 == "U" and base2 == "A"
    return match

def is_transition(base1, base2):
    transition = base1 == "G" and base2 == "A"
    transition |= base1 == "A" and base2 == "G"
    transition |= base1 == "C" and base2 == "T"
    transition |= base1 == "T" and base2 == "C"
    return transition

def is_transversion(base1, base2):
    return (base1 != base2) and not is_transition(base1, base2)
