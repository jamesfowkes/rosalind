import unittest
from unittest.mock import patch
from io import StringIO

from stdinreader import read_stdin, read_parts, read_one_line

class StdinReaderTest(unittest.TestCase):

    @patch("sys.stdin", StringIO("1 2 3\r\n"))
    def testReadOneLineReturnsStrippedLine(self):
        self.assertEqual("1 2 3", read_stdin(read_one_line))

if __name__ == '__main__':
    unittest.main()
