import sys


def read_one_line(iterator):
    return next(iterator).strip()


def read_parts(part_type, number_of_parts=None):

    def read_part_fn(iterator):
        string = read_one_line(iterator)
        parts = string.split(" ")

        if number_of_parts == 1:
            return part_type(parts[0])
        else:
            return tuple(part_type(p) for p in parts)

    return read_part_fn


def read_stdin(func_or_func_list, repeat_to_end_fn=None):

    iterator = iter(sys.stdin.readlines())

    results = []
    end_results = []

    if callable(func_or_func_list):
        results = func_or_func_list(iterator)
    else:
        for f in func_or_func_list:
            results.append(f(iterator))

    if repeat_to_end_fn is not None:
        try:
            while True:
                end_results.append(repeat_to_end_fn(iterator))
        except StopIteration:
            pass

        return results, end_results
    else:
        return results


def read_lines(function=None):

    if function is None:
        function = lambda s: s.strip()

    def read_line_fn(iterator):
        return [function(i) for i in iterator]

    return read_line_fn
