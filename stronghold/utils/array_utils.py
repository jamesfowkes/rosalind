def make_array(*args, **kwargs):
    n_dimensions = len(args)
    fill_value = kwargs.get("fill_value", None)

    if n_dimensions == 1:
        array = [fill_value] * args[0]
    else:
        array = [make_array(*args[1:], fill_value=fill_value) for n in range(args[0])]

    return array
