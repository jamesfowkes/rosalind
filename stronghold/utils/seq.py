from dataclasses import dataclass

try:
    import base
    from string_utils import find_substrings
except Exception:
    from . import base
    from .string_utils import find_substrings


class Sequence:
    pass


@dataclass
class ReadingFrame:
    original: object
    start: int
    stop: int
    reverse: bool
    seq: str

    def protein(self):
        new_sequence = type(self.original)(self.original.name, self.seq)
        return new_sequence.protein()


@dataclass(init=False)
class Sequence:
    name: str
    seq: str
    reverse: bool

    def __init__(self, name: str, seq: str, reverse: bool = False):
        self.name = name
        self.seq = "".join([l.strip() for l in seq.splitlines()])
        self.reverse = reverse

    def __getitem__(self, index):
        return self.seq[index]

    def reverse_complement(self) -> Sequence:
        reversed = list(self.seq[::-1])
        complemented = [base.complement_base(b) for b in reversed]
        return type(self)(self.name, ''.join(complemented), not self.reverse)

    def gc_content(self) -> float:
        c_count = self.seq.count("C")
        g_count = self.seq.count("G")
        return (c_count + g_count) * 100 / len(self.seq)

    def __len__(self):
        return len(self.seq)

    def hamming_distance(self, other) -> int:

        if isinstance(other, Sequence):
            other = other.seq

        if len(self) != len(other):
            raise Exception("Length of sequences must be equal ({} != {}".format(len(self), len(other)))

        hamming_distance = 0
        for c1, c2 in zip(self.seq, other):
            if c1 != c2:
                hamming_distance += 1

        return hamming_distance

    def tt_ratio(self, other) -> float:

        if isinstance(other, Sequence):
            other = other.seq

        if len(self) != len(other):
            raise Exception("Length of sequences must be equal ({} != {}".format(len(self), len(other)))

        transitions = 0
        transversions = 0

        for c1, c2 in zip(self.seq, other):
            if base.is_transition(c1, c2):
                transitions += 1
            elif base.is_transversion(c1, c2):
                transversions += 1

        return transitions/transversions

    def find(self, motif):
        locations = []
        search_index = 0

        while True:
            result = self.seq[search_index:].find(motif)
            if result > -1:
                location = search_index + result
                locations.append(location)
                search_index = location + 1
            else:
                break

        return locations

    def __hash__(self):
        return hash(self.seq)

    def get_orfs(self):

        starts = self.find_start_codons()
        stops = self.find_stop_codons()

        orfs = []
        for start in starts:
            for stop in stops:
                if (stop > start) and (((start - stop) % 3) == 0):
                    orfs.append(ReadingFrame(
                        self,
                        start, stop,
                        self.reverse,
                        self.seq[start:stop+3]
                        )
                    )
                    break

        return tuple(orfs)

    def open_reading_frames(self):
        fwd_orfs = self.get_orfs()
        rev_orfs = self.reverse_complement().get_orfs()
        return (*fwd_orfs, *rev_orfs)

    def find_start_codons(self):
        return find_substrings(self.seq, self.START_CODON)

    def find_stop_codons(self):
        tags = find_substrings(self.seq, self.STOP_CODONS[0])
        tgas = find_substrings(self.seq, self.STOP_CODONS[1])
        taas = find_substrings(self.seq, self.STOP_CODONS[2])
        return tuple(sorted((*tags, *tgas, *taas)))

    def subseq(self, start=0, stop=None):
        if stop:
            seq = self.seq[start:stop]
        else:
            seq = self.seq[start:]

        return type(self)(self.name+"_subseq{}:{}".format(start, stop), seq)

    def match(self, other):
        return self.seq == other.seq or self.seq == other.reverse_complement().seq


class ProfileMatrix:

    def __init__(self, sequences):
        lengths = (len(s) for s in sequences)
        if len(set(lengths)) != 1:
            raise Exception("All sequences must be of equal length to create profile matrix")

        self.length = len(sequences[0])
        counts = [[0] * self.length for _ in range(4)]

        for s in sequences:
            for i, c in enumerate(s.seq):
                if c == 'A':
                    counts[0][i] += 1
                elif c == 'C':
                    counts[1][i] += 1
                elif c == 'G':
                    counts[2][i] += 1
                elif c == 'T':
                    counts[3][i] += 1

        self.matrix = tuple(tuple(c) for c in counts)

        consensus = [''] * self.length

        for i in range(self.length):
            counts = [self.matrix[c][i] for c in range(4)]
            max_index = counts.index(max(counts))
            consensus[i] = "ACGT"[max_index]

        self.consensus = Sequence("", "".join(consensus))

    def __len__(self):
        return self.length


CODON_TO_PROTEIN_TABLE = {
    "UUU": "F",    "CUU": "L", "AUU": "I", "GUU": "V",
    "UUC": "F",    "CUC": "L", "AUC": "I", "GUC": "V",
    "UUA": "L",    "CUA": "L", "AUA": "I", "GUA": "V",
    "UUG": "L",    "CUG": "L", "AUG": "M", "GUG": "V",
    "UCU": "S",    "CCU": "P", "ACU": "T", "GCU": "A",
    "UCC": "S",    "CCC": "P", "ACC": "T", "GCC": "A",
    "UCA": "S",    "CCA": "P", "ACA": "T", "GCA": "A",
    "UCG": "S",    "CCG": "P", "ACG": "T", "GCG": "A",
    "UAU": "Y",    "CAU": "H", "AAU": "N", "GAU": "D",
    "UAC": "Y",    "CAC": "H", "AAC": "N", "GAC": "D",
    "UAA": "Stop", "CAA": "Q", "AAA": "K", "GAA": "E",
    "UAG": "Stop", "CAG": "Q", "AAG": "K", "GAG": "E",
    "UGU": "C",    "CGU": "R", "AGU": "S", "GGU": "G",
    "UGC": "C",    "CGC": "R", "AGC": "S", "GGC": "G",
    "UGA": "Stop", "CGA": "R", "AGA": "R", "GGA": "G",
    "UGG": "W",    "CGG": "R", "AGG": "R", "GGG": "G"
}


PROTEIN_TO_CODON_TABLE = {
    "F": ["UUU", "UUC"],
    "L": ["CUU", "CUC", "UUA", "UUG", "CUA", "CUG"],
    "I": ["AUU", "AUC", "AUA"],
    "V": ["GUU", "GUC", "GUA", "GUG"],
    "M": ["AUG"],
    "S": ["UCU", "UCC", "UCA", "UCG", "AGU", "AGC"],
    "P": ["CCU", "CCC", "CCA", "CCG"],
    "T": ["ACU", "ACC", "ACA", "ACG"],
    "A": ["GCU", "GCC", "GCA", "GCG"],
    "Y": ["UAU", "UAC"],
    "H": ["CAU", "CAC"],
    "N": ["AAU", "AAC"],
    "D": ["GAU", "GAC"],
    "Q": ["CAA", "CAG"],
    "K": ["AAA", "AAG"],
    "E": ["GAA", "GAG"],
    "C": ["UGU", "UGC"],
    "R": ["CGU", "CGC", "CGA", "AGA", "CGG", "AGG"],
    "G": ["GGU", "GGC", "GGA", "GGG"],
    "W": ["UGG"],
    "Stop": ["UAA", "UAG", "UGA"]
}

PROTEIN_MASS_TABLE = {
    "A": 71.03711,
    "C": 103.00919,
    "D": 115.02694,
    "E": 129.04259,
    "F": 147.06841,
    "G": 57.02146,
    "H": 137.05891,
    "I": 113.08406,
    "K": 128.09496,
    "L": 113.08406,
    "M": 131.04049,
    "N": 114.04293,
    "P": 97.05276,
    "Q": 128.05858,
    "R": 156.10111,
    "S": 87.03203,
    "T": 101.04768,
    "V": 99.06841,
    "W": 186.07931,
    "Y": 163.06333
}


class ProteinSequence(Sequence):

    def get_candidate_rna(self, index):
        if index < len(self):
            return PROTEIN_TO_CODON_TABLE[self.seq[index]]
        else:
            raise Exception("Requested index {} out of bounds (max {})".format(index, len(self)-1))

    def mass(self):
        return sum(PROTEIN_MASS_TABLE[c] for c in self.seq)


class RNASequence(Sequence):

    START_CODON = "AUG"
    STOP_CODONS = ["UAA", "UAG", "UGA"]

    def protein(self):

        def split(string, n):
            for i in range(0, len(string), n):
                yield string[i:i + n]

        codons = split(self.seq, 3)

        amino_acids = []
        for c in codons:
            if CODON_TO_PROTEIN_TABLE[c] == "Stop":
                break
            else:
                amino_acids.append(CODON_TO_PROTEIN_TABLE[c])

        return ProteinSequence(self.name, "".join(amino_acids))

    def to_dna(self):
        return self.seq.replace("U", "T")

    def acgu_count(self) -> int:
        return [self.seq.count(c) for c in "ACGU"]

    def match_bases(self, index1, index2):
        """ Return true if bases at index1 and index2 are complements. Zero-indexed. """
        return base.match_rna(self.seq[index1], self.seq[index2])


class DNASequence(Sequence):

    START_CODON = "ATG"
    STOP_CODONS = ["TAA", "TAG", "TGA"]

    def to_rna(self):
        return RNASequence(self.name, self.seq.replace("T", "U"))

    def protein(self):
        return self.to_rna().protein()

    def acgt_count(self) -> int:
        return [self.seq.count(c) for c in "ACGT"]