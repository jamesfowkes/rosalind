import unittest

from string_utils import failure_array

class FailureArrayTest(unittest.TestCase):

    def test_SimpleFailureArraysWithAllZeros(self):
        self.assertEqual([], failure_array(""))
        self.assertEqual([0], failure_array("a"))
        self.assertEqual([0, 0, 0], failure_array("abc"))
        # Even though "aa" prefixes and suffixes match, there's no point matching on solely the last character
        # as the entire string would match
        self.assertEqual([0, 0], failure_array("aa"))
        self.assertEqual([0, 0, 0, 0, 0, 0, 0], failure_array("abcdefa"))

    def test_SimpleFailureArraysWithAllOnes(self):
        self.assertEqual([0, 0, 0, 0, 1, 0, 0, 0], failure_array("abcdaefa"))

    def test_ComplexFailureArrays(self):
        self.assertEqual([0, 0, 0, 1, 2, 0, 0, 0, 1, 2, 3, 4, 5, 3, 0], failure_array("abcabdefabcabcd"))
        self.assertEqual([0, 0, 0, 1, 2, 0, 0, 0, 0, 0, 0, 1, 2, 1, 2, 3, 4, 5, 3, 0, 0], failure_array("CAGCATGGTATCACAGCAGAG"))
        self.assertEqual([0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 0, 0, 0, 1, 0, 0], failure_array("CAGTAAGCAGGGACTG"))

if __name__ == '__main__':
    unittest.main()
