try:
    from num_utils import is_odd
    from print_utils import print_nums
except ImportError:
    from .num_utils import is_odd
    from .print_utils import print_nums

def find_substrings(s, subs):

    indexes = []
    find_index = 0

    while find_index >= 0:
        find_index = s.find(subs, find_index)
        if find_index >= 0:
            indexes.append(find_index)
            find_index += 1

    return tuple(indexes)

from itertools import zip_longest
def merge(istr, jstr, ignore, match_char="*"):

    out = []
    for i, j in zip_longest(istr, jstr, fillvalue=ignore):
        if i == ignore and j == ignore:
            out.append(ignore)
        elif i == j:
            out.append(match_char)
        elif i != ignore:
            out.append(i)
        elif j != ignore:
            out.append(j)

    return "".join(out)

def failure_array(s, debug=False):

    n = len(s)

    if len(s) == 0:
        return []

    i = -1
    j = 0
    
    failure_array = [0] * n

    failure_array[j] = i

    while j < n:
        while (i >= 0) and (s[j] != s[i]):
            # i is the index to match against (the end of the current prefix under test)
            # if there is no match here, then push i back to the value in the array
            # Because we've matched up to the ith location already, and there 
            # might be smaller prefix matches already stored that would
            # save us going back to the beginning of the string
            i = failure_array[i]

        j += 1
        i += 1
        if j < n:
            # The longest prefix at the jth location is the value of the i-index
            failure_array[j] = i

        if debug:
            istr = " " * (((i-1)*2)) + "i"
            jstr = " " * (((j-1)*2)) + "j"
            print(merge(istr, jstr, " "))
            print_nums(failure_array[1:])
            print(" ".join(s))
            print()

    failure_array = failure_array[1:]
    failure_array.append(0)

    return failure_array
