def is_odd(n):
    return bool(n & 1)

def is_even(n):
    return not bool(n & 1)
