def print_nums(arr, sep=" "):
    print(sep.join([str(a) for a in arr]))

def print_array_lines(arr):
    for a in arr:
        print(a)

def print_twodim_array(arr):
    for i in range(len(arr)):
        for j in range(len(arr[0])):
            print(arr[i][j], end='')
            print("    ", end='')
        print("")
