try:
    from string_utils import *
except ImportError:
    from .string_utils import *

try:
    from num_utils import *
except ImportError:
    from .num_utils import *

try:
    from print_utils import *
except ImportError:
    from .print_utils import *

try:
    from iter_utils import *
except ImportError:
    from .iter_utils import *

