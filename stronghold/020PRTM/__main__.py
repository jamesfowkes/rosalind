from stronghold.utils.stdinreader import read_stdin, read_one_line
from stronghold.utils.seq import ProteinSequence

seq = ProteinSequence("", read_stdin(read_one_line))

print(seq.mass())
