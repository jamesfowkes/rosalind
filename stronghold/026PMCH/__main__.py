import sys
import math

from stronghold.utils.fasta import parse_fasta
from stronghold.utils.seq import RNASequence

sequence = parse_fasta(RNASequence, sys.stdin.readlines())

count_a = sequence.seq.count("A")
count_c = sequence.seq.count("C")

print(math.factorial(count_a) * math.factorial(count_c))
