import unittest

from DNA import count_acgt

class DNATest(unittest.TestCase):

    def testZeroLengthStringReturnsAllZeros(self):
        assert [0, 0, 0, 0] == count_acgt("")

    def testStringWithNoValidCharsReturnsAllZeros(self):
        assert [0, 0, 0, 0] == count_acgt("BDEFHIJKLMNOPQRSUVWXYZ")

    def testValidStringReturnsExpectedCounts(self):
        assert [20, 12, 17, 21] == count_acgt("AGCTTTTCATTCTGACTGCAACGGGCAATATGTCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGC")

if __name__ == '__main__':
    unittest.main()