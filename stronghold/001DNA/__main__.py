from stronghold.utils.stdinreader import read_stdin, read_one_line
from stronghold.utils.seq import DNASequence

seq = DNASequence("", read_stdin(read_one_line))

print(" ".join(str(i) for i in seq.acgt_count()))
