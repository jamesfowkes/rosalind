# rosalind

Python solutions to the bioinformatics problems at rosalind.info

To run a problem: python3 -m <problem_set> <problem_number> [input_file]

where:

* problem_set is one of "stronghold", "armory" or "textbook" (currently only stronghold is supported)
* problem_number is the number of the problem starting at 1
* input_file is the optional input file to use (must be in the same folder as the problem solution). Default: input.txt
